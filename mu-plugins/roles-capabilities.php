<?php
/* 
Plugin Name: Roles and Capabilities
Description: Custom Roles and capabilities for Troo
Author: everythingdifferent
Version: 1.0
Author URI: http://everythingdifferent.co.uk
*/


remove_role( 'subscriber' );
remove_role( 'contributor' );
remove_role( 'author' );


?>
