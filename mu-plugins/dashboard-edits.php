<?php
/*
Plugin Name: Custom Dashboard
Author: everythingdifferent
Version: 1.0
Author URI: http://everythingdifferent.co.uk
*/

/* 
 * Appearance permissions: edit Appearance menu for selected roles
 * Author: Vladimir Garagulya
 * email: support@role-editor.com
 * 
 */
 

function wpb_custom_logo() {
echo '
<style type="text/css">
.wp-admin #wpadminbar #wp-admin-bar-site-name>.ab-item:before {
    content:"";
}
</style>
';
}

//hook into the administrative header output
add_action('wp_before_admin_bar_render', 'wpb_custom_logo');

class AppearancePermissions {
 
    // roles to which allow the Appearance menu
    private $allowed_roles = array('editor');
    // allowed items from Appearance menu
    private $allowed_items = array(
        'widgets.php',
        'nav-menus.php'
    );
    // capabilities required by WordPress to access to the menu items above
    private $required_capabilities = array(
      'edit_theme_options'  
    );
 
    private $prohibited_items = array();

}
add_action( 'admin_bar_menu', 'remove_wp_admin_elements', 999 );

function remove_wp_admin_elements( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );	
	$wp_admin_bar->remove_node( 'comments' );
	
	if ( current_user_can( 'editor' ) || current_user_can('press') ) { //editor user roles
		$wp_admin_bar->remove_node( 'wpseo-menu' );
	}	
}

add_action( 'admin_menu', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
	//all user roles
	remove_menu_page('edit-comments.php');	
	remove_menu_page('ajax-load-more', 'ajax-load-more');	

	if ( current_user_can( 'editor' ) || current_user_can('press') ) { //editor user roles
		remove_menu_page('tools.php');	
		remove_menu_page('tools.php');	
		remove_menu_page('index.php');	
		// remove_submenu_page( 'themes.php', 'themes.php' ); // hide the theme selection submenu
	    // remove_submenu_page( 'themes.php', 'widgets.php' ); // hide the widgets submenu


	}
}

function ja_remove_customizer_options( $wp_customize ) {
   //$wp_customize->remove_section( 'static_front_page' );
   //$wp_customize->remove_section( 'title_tagline'     );
   //$wp_customize->remove_section( 'nav'               );
   $wp_customize->remove_section( 'themes'              );
}
add_action( 'customize_register', 'ja_remove_customizer_options', 30 );
// add_action( 'admin_init', 'wpse_136058_debug_admin_menu' );

function wpse_136058_debug_admin_menu() {

    echo '<pre>' . print_r( $GLOBALS[ 'menu' ], TRUE) . '</pre>';
}
function remove_customize() {
    $customize_url_arr = array();
    $customize_url_arr[] = 'customize.php'; // 3.x
    $customize_url = add_query_arg( 'return', urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ), 'customize.php' );
    $customize_url_arr[] = $customize_url; // 4.0 & 4.1
    if ( current_theme_supports( 'custom-header' ) && current_user_can( 'customize') ) {
        $customize_url_arr[] = add_query_arg( 'autofocus[control]', 'header_image', $customize_url ); // 4.1
        $customize_url_arr[] = 'custom-header'; // 4.0
    }
    if ( current_theme_supports( 'custom-background' ) && current_user_can( 'customize') ) {
        $customize_url_arr[] = add_query_arg( 'autofocus[control]', 'background_image', $customize_url ); // 4.1
        $customize_url_arr[] = 'custom-background'; // 4.0
    }
    foreach ( $customize_url_arr as $customize_url ) {
        remove_submenu_page( 'themes.php', $customize_url );
    }
}
add_action( 'admin_menu', 'remove_customize', 999 );


// EDIT THE DASHBOARD

function disable_default_dashboard_widgets() {

    // remove_meta_box('dashboard_right_now', 'dashboard', 'core');
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'core');
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');
    remove_meta_box('dashboard_plugins', 'dashboard', 'core');
    // remove_meta_box('dashboard_activity', 'dashboard', 'core');

    remove_meta_box('dashboard_quick_press', 'dashboard', 'core');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');
    remove_meta_box('dashboard_primary', 'dashboard', 'core');
    // remove_meta_box('dashboard_secondary', 'dashboard', 'core');
}
add_action('admin_menu', 'disable_default_dashboard_widgets');



add_action( 'wp_before_admin_bar_render', 'control_toolbar_content' ); 

function control_toolbar_content()
{
    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('customize');
}

?>
