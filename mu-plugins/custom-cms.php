<?php
/* 
Plugin Name: Customised CMS
Description: Customised CMS for Troo
Author: everythingdifferent
Version: 1.0
Author URI: http://everythingdifferent.co.uk
*/


	// TRY THIS: https://torquemag.io/2016/08/customize-wordpress-backend-clients/
	function custom_login_page() { ?>
	    <style type="text/css">
	    
	    /* CUSTOMISE LOGIN PAGE */
		    body.login { background-color:#2E1A47; }
			body.login div#login {}
			body.login div#login h1 {}
			body.login div#login h1 a { background-size:170px; width:100%; }
			body.login div#login form#loginform {
				padding: 35px 28px;
			}
			body.login div#login form#loginform p {}
			body.login div#login form#loginform p label {}
			body.login div#login form#loginform input {}
			body.login div#login form#loginform input#user_login {}
			body.login div#login form#loginform input#user_pass {}
			body.login div#login form#loginform p.forgetmenot {}
			body.login div#login form#loginform p.forgetmenot input#rememberme {}
			body.login div#login form#loginform p.submit {}
			body.login div#login form#loginform p.submit input#wp-submit { 
				background-color:#7474C1;
			    border-color: transparent;
			    -webkit-box-shadow: 0 1px 0 #006799;
			    box-shadow: none;
			    color: #fff;
			    text-decoration: none;
			    text-shadow: none;

			}
			body.login div#login form#loginform p.submit input#wp-submit:hover { 

			}
			body.login div#login p#nav {}
			body.login div#login p#nav a { color:#D9D9D6;}
			body.login div#login p#nav a:hover { color:#ffffff;}
			body.login div#login p#backtoblog {}
			body.login div#login p#backtoblog a { color:#888B8D;}
			body.login div#login p#backtoblog a:hover { color:#ffffff;}
	        #login h1 a, .login h1 a {
	            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/troo-logo.png);
	            padding-bottom: 30px;
	        }

	        .login form .input, .login form input[type=checkbox], .login input[type=text] {
	        	color:#7474C1;
	        }
	    </style>
	<?php }
	
	add_action( 'login_enqueue_scripts', 'custom_login_page' );

	wp_admin_css_color(
	    'colour-handle', __( 'Troo', 'admin_schemes' ), // for admin screen only
	    plugins_url( 'colors/troo/colors.css', __FILE__ ), // go here for actual colours
	    array( '#aaaaaa', '#FF6C1C', '#7D6B5C', '#456a7f' ), // for admin screen only
	    array( 'base' => '#FF6C1C', 'focus' => '#FFFFFF', 'current' => '#CCCCCC' ) // for admin screen only
	);

	// REMOVES ABILITY TO EDIT FILES FROM WITHIN CMS
	define( 'DISALLOW_FILE_EDIT', true );

add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
	return get_site_url();
}


// SET DEFAULT COLOUR SCHEME FOR NEW USERS
function set_default_admin_color($user_id) {
	$args = array(
		'ID' => $user_id,
		'admin_color' => 'colour-handle'
	);
	wp_update_user( $args );
}
add_action('user_register', 'set_default_admin_color');

function theme_prefix_setup() {
	
	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-width' => true,
	) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );


?>
